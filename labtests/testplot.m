% Labtest data

for i=1:3
    rfile = sprintf('EITOR_run%d.dat',i);
    tfile = sprintf('run%d.txt',i);
    rdata = dlmread(rfile,'',2,0);
    tdata = dlmread(tfile,',',1,0);
    
    figure
    plot(rdata(:,1),rdata(:,2),tdata(:,1),tdata(:,2));
    xlabel('Frequency (kHz)');
    ylabel('Impedance (Ohms)');
    xlim([1,100]);
    grid on;

end


