#!/usr//bin/python
"""Description: Python script for determining calibration values for AD5933
Date: 05/25/2014
Notes: Ensure onboard atmel mcu is flashed with ic_control.ino sketch
IC Specs:
    Frequency Range: 1KHz  - 100KHz
    Impedance Range: 1KOhm - 10MOhm
    
Note, Feedback Resistor, RFB should be tailored for specific impedance range
"""

## Computing RFB
#Rfb = input("Enter Rfb value in KOhms: ");
Range = input("Select operation range [1-4]: ");
Vdd  = input("Enter Vdd value in volts: ");
Gain = input("Enter PGA gain: ");
Zmin = input("Enter Zmin in KOhms: ");
Zmax = input("Enter Zmax in KOhms: ");

if Range==1:
    Vpk   = 1.98 #Vpp
    Vdc   = 1.48 #Vpp
    Zout  = 0.2 #KOhms
    MRate = 40
elif Range==2:
    Vpk   = 0.97
    Vdc   = 0.76
    Zout  = 2.4 #KOhms
    MRate = 15
elif Range==3:
    Vpk   = 0.383
    Vdc   = 0.31
    Zout  = 1 #KOhms    
    MRate = 5

elif Range==4:
    Vpk   = 0.198
    Vdc   = 0.173
    Zout  = 0.6 #KOhms    
    MRate = 2

Rfb = (((Vdd/2 -0.2)*Zmin)/(Vpk + Vdd/2 - Vdc))*1/Gain
Rcal = (Zmin+Zmax)*1/3


if Zmax/Zmin < MRate:
    print "Desired range fits selected range {} ratio".format(Range)
else:
    print "Range, ratio {} DOES NOT fit selected range {}!!".format(Zmax/Zmin,Range)
    
print "Choose Rcal: {} KOhms".format(Rcal)
print "Choose Rfb: {} KOhms".format(Rfb)
   
            