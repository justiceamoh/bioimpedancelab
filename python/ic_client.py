#!/usr/bin/python
"""
Author: Justice Amoh
Description: Python script to communicate with embedded impedance analyzer system
Date: 05/20/2014
Notes: Ensure onboard atmel mcu is flashed with ic_control.ino sketch
IC Specs:
    Frequency Range: 1KHz  - 100KHz
    Impedance Range: 1KOhm - 10MOhm
    
Note, Feedback Resistor, RFB should be tailored for specific impedance range
"""


import serial
import matplotlib.pyplot as plt
import re
import pickle

# Controls
calibrate = False
adjust    = True
save      = True

# Declare objects and variables
#ser   = serial.Serial('/dev/tty.usbmodemfd121',115200,timeout=5)
ser = serial.Serial('/dev/ttyACM0',115200,timeout=5)
T     = 0
Freq  = []
Mag   = []
R     = []
X     = []
Imped = []

Zcal = 1195 #Calibration impedance in Ohms

## Display Script Control
def help():
    print ""
    print "Impedance IC Control Script v0.1"
    print "Control Commands"
    print "Measure Temperature: 1"
    print "Run frequency sweep: 2"
    print "Program registers  : 3"
    print "Display this info  : 4"
    print "Exit or quit       : 5"
    print "" 
    
    
## Measure Temperature
def measure_temp():
    ser.write('B')
    val = ser.readline()
    T   = val[13:-4]
    pass
        

def run_sweep():
    ser.write('C')
    #while ser.inWaiting():
    for i in range(100):
        line = ser.readline()
        line = re.split(r'[:,]+',line)
        Freq.append(float(line[1]));
        Mag.append(float(line[3]));
        R.append(float(line[5]));
        X.append(float(line[7]));
    
# save if calibration
    if calibrate:
        #data = [Freq,Imped,Mag,Phase]        
        with open("cal_data", 'wb') as f:
            pickle.dump(Mag, f)

# adjust magnitude with Zcal
    if adjust:
        with open("cal_data", 'rb') as f:
            data = pickle.load(f)
         
        for i in range(len(data)):
            gain = (1.0/Zcal)/data[i]
            Imped.append(1/(gain*Mag[i]))                 

# save data to file
    if save:
        f = open('run_data.txt', 'w')
        print>>f, 'Frequency,Magnitude,Resistance,Reactance'
        for i in range(len(Mag)):
            print>>f, Freq[i],',',Mag[i],',',R[i],',',X[i]    
        
          
# plot data
    plt.plot(Freq,Imped,'-bo')
    plt.ylabel("Impedance (Ohms)")
    plt.xlabel("Frequency (KHz)")
    plt.xlim(xmin=1)
    plt.show()                                                        
    pass

        
def program_reg():
    #ser.write('A')
    pass



# Main FSM
#help()
#while True:
#    case = input("Enter command: ")
#    if case == 1:
#        measure_temp()
#    elif case == 2:
#        run_sweep()
#        break
#    elif case == 3:
#        program_reg()
#    elif case == 4:
#        help()            
#    elif case == 5:
#        print "Exiting program."
#        ser.close()
#        break
            
run_sweep()