% Probe properties
A = 2.677; %cm^2, 58mm circumference
L = 0.5; %cm


% Read file.
filename = {'Run1.txt', 'Run2.txt', 'Run3.txt','Run4.txt', 'Run5.txt'};
for i=1:length(filename);
    data = csvread(filename{i},3,0);
    file=fopen(filename{i});
    fgetl(file);
    conductivity = fgetl(file); fclose(file);


    % Variables
    Frequency = data(:,1);
    Impedance = data(:,2);
    Magnitude = data(:,3);

    subplot(3,2,i);
    plot(Frequency,Impedance);
    grid on;
    title(conductivity);
    xlabel('Frequency (KHz)');
    ylabel('Impedance (kOhms)');

end

rho = Impedance .* A/L;