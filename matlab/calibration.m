% Author: Justice Amoh
% Description: Matlab script to calculate values for calibration resistors, Rfb and Zcal for AD5933
% Date: 07/02/2014
% IC Specs:
%     Frequency Range: 1KHz  - 100KHz
%     Impedance Range: 1KOhm - 10MOhm

%% Computing Calibration Values
Range = 1; 						% Operation range [1-4]
Vdd	  = 3.3;					% Source voltage, vdd in volts
Gain  = 1;						% PGA Gain [1 or 5]
Zmin  = 200;					% Minimum impedance in ohms
Zmax  = 800;					% Maximum impedance in ohms

switch Range
	case 1
		Vpk	   = 1.98;
		Vdc	   = 1.48;
		Zout   = 200;
		MRatio = 40;
	case 2
		Vpk	   = 0.97;
		Vdc	   = 0.76;
		Zout   = 2.4;
		MRatio = 15;
	case 3
		Vpk	   = 0.383;
		Vdc	   = 0.31;
		Zout   = 1000;
		MRatio = 5;
	case 4
		Vpk	   = 0.198;
		Vdc	   = 0.173;
		Zout   = 600;
		MRatio = 2;
end

Zmin = Zmin + Zout;
Zmax = Zmax + Zout; 

Rfb = (((Vdd/2 - 0.2)*Zmin)/(Vpk + Vdd/2 - Vdc))*1/Gain;
Zcal = (Zmin + Zmax)*1/3;

if Zmax/Zmin > MRatio
	disp('Range ratio DOES NOT fit selected range!!');
	return;
end

Rfb
Zcal