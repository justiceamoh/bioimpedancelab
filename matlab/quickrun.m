% Author: Justice Amoh
% Description: Matlab script to quickly run frequency sweep on ad5933 via serial to arduino
% Date: 07/02/2014
% Notes: - Ensure onboard atmel mcu is flashed with ic_control.ino sketch
%		 - Also calibrate using Rfb and Zcal from calibration script
% IC Specs:
%     Frequency Range: 1KHz  - 100KHz
%     Impedance Range: 1KOhm - 10MOhm

clear all;

% Controls
calibrate = false;
adjust	  = true;
savedat	  = true;


% Constants
Zcal = 465;						% Calibration impedance in ohms
comPort='/dev/tty.usbmodemfd121';	% USB Serial Port

% Initializations
T     = 0;
Freq  = [];
Mag   = [];
R     = [];
X     = [];
Imped = [];

% Serial Setup
s = serial(comPort);
set(s,'Databits',8);
set(s,'StopBits',1);
set(s,'BaudRate',115200);
set(s,'Parity','none');


% Run Sweep
fopen(s);
pause(2);

fwrite(s,'C');
pause(1);
i = 1;
while(1)
    if(s.BytesAvailable)
    dummy = fscanf(s,'%s');
    dummy = strsplit(dummy,{':',','});
    Freq(i) = str2double(dummy{2});
    Mag(i) = str2double(dummy{4});
    R(i) = str2double(dummy{6});
    X(i) = str2double(dummy{8});
    
    i=i+1;
    end
    
    
    if(i>100)  % Check if all frequency points have been received
        break;
    end
        
end

fclose(s);
delete(s);


if calibrate
    cal_data = [Freq' Mag' R' X'];
    save cal_data cal_data;
end


if adjust
    load cal_data;
    gf  = (1/Zcal)./cal_data(:,2);
    Imped =  1./(gf'.*Mag);
end    


plot(Freq,Imped);
xlabel('Frequency (Khz)');
ylabel('Impedance (Ohms)');
grid on;

